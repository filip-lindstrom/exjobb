generella kommentarer
ha alltid sm� bokst�ver p� mappar (detta �r standard) ...
Man d�per vanligtvis source och include mappar till src/include
i regel checkar man aldrig in bygg mappar. Man bygger bin�rer och libbar lokalt.
variabler och funktioner brukar b�rja med liten bokstav. Klasser och struktar med stor
Det �r lite olika tabl�ngder i dina funktioner. Vanligtvis n�r man �r flera i projekt �r det r�tt bra att 
ha en ny tabb vid ny if sats / loop / funktion. L�tt att det blir on�diga mergekonflikter annars

makefile
ist�llet f�r att manuellt l�gga till c-filer i makefilen kan man klumpa ihop dem
till en variabel, via kommandot

$(wildcard *.c)

P� s� s�tt beh�ver du modda makefilen d� du l�gger till en fil eller byter namn,
(�r inte s�ker p� syntaxen)

i distance.c
Du har n�gra static variabler. 
dessa kommer ha kvar sitt v�rde fr�n tidigare anrop. Kan vara bra att veta om du inte visste
Dessa verkar inte initieras


mpu.c
i  Skapar du konstanter via #defines, defines anv�nds d� man vill ha info i pre-compilering.
b�ttre att ha const int MY_CONSTANT = x ist�llet f�r #define MY_CONSTANT x.

du har en init funktion som initierar hi2c1, detta g�r att g�ra / delvis g�ra,
vid skapandet av variabeln med syntaxen
MY_TYPE a = { .flag = true, .value = 123, .stuff = 0.456 };

pwm.c
int convertToPWM(int value)
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    return value;
}

Den h�r funktionen ger nog inte ut r�tt svar om value = 101, b�ttre att kasta ett exception (om man nu kan 
g�ra det i c :P)

state.c
inkluderar main.h, endast main ska inkludera main.h
runHW(void), g�rs snyggare med switch case sats ist�llet f�r if/else, den har heller ingen else

stm32f1xx*.c
borde du kunna g�ra ett bibliotek av och l�nka in om du inte t�nkte modda i den (antar att det �r 3e parts mjukvara)