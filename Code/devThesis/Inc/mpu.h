/*
 * mpu.h
 *
 *  Created on: 6 apr. 2018
 *      Author: virtual
 */

#ifndef INC_MPU_H_
#define INC_MPU_H_

/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
bool initMpu(void);
void readMpuAll(void);
void readMpuGyro(void);
void readMpuAcc(void);
void readMpuTemp(void);
int getGyroX(void);
int getGyroY(void);
int getGyroZ(void);
int getAccX(void);
int getAccY(void);
int getAccZ(void);
int getTemp(void);
int testAverageValue(int value);


/* Public variables */


#endif /* INC_MPU_H_ */
