/*
 * state.h
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

#ifndef INC_STATE_H_
#define INC_STATE_H_

/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
void runStatemachine(void);
void setHwState(int value);
int getHwState(void);
int getCurrentState(void);
int getDistanceDown(void);
int getDistanceFront(void);

/* Public variables */

#endif /* INC_STATE_H_ */
