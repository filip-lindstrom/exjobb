/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "stm32f1xx_hal.h"
#include "directions.h"
#include "state.h"
#include "distance.h"
#include "handle.h"
#include "pwm.h"
#include "mpu.h"

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define OnboardLed_Pin         GPIO_PIN_13
#define OnboardLed_GPIO_Port   GPIOC
#define Throttle_Pin           GPIO_PIN_4
#define Throttle_GPIO_Port     GPIOA
#define Rudder_Pin             GPIO_PIN_5
#define Rudder_GPIO_Port       GPIOA
#define Elevator_Pin           GPIO_PIN_6
#define Elevator_GPIO_Port     GPIOA
#define Aileron_Pin            GPIO_PIN_7
#define Aileron_GPIO_Port      GPIOA
#define WifiDetected_Pin       GPIO_PIN_4
#define WifiDetected_GPIO_Port GPIOB
#define WifiNear_Pin           GPIO_PIN_5
#define WifiNear_GPIO_Port     GPIOB
#define RedLed_Pin             GPIO_PIN_8
#define RedLed_GPIO_Port       GPIOB
#define YellowLed_Pin          GPIO_PIN_9
#define YellowLed_GPIO_Port    GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
// Debug flags
#define DEBUG_DIST  0
#define DEBUG_TIME  0
#define DEBUG_STATE 0
#define DEBUG_WIFI  0
#define DEBUG_MPU   0

#define FALSE 0
#define TRUE  1

/* Public functions */
void com_c(int ComPort, char string[]);
void com_ci(int ComPort, char string[], int var);
int getAdc(int AdcNo);
int getUart2(void);
void delayUs(long unsigned int us);

/* Public variables */
enum {
	PC = 1,
	BT = 2
};



/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
