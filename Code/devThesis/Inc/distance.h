/*
 * distance.h
 *
 *  Created on: 22 mars 2018
 *      Author: virtual
 */

#ifndef INC_DISTANCE_H_
#define INC_DISTANCE_H_

/* Public includes */

/* Defines */
#define FILTERSIZEDOWN 20
#define FILTERSIZEFRONT 10

/* Public declarations */

/* Public functions */
int readSensors(int Dir);
void initFilters(int valueFront, int valueDown);

/* Public variables */
enum {
    Down  = 0,
    Front = 1
};

#endif /* INC_DISTANCE_H_ */
