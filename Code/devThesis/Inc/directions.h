/*
 * directions.h
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

#ifndef INC_DIRECTIONS_H_
#define INC_DIRECTIONS_H_

/* Public includes */

/* Defines */
//Height parameters
#define LAND_HEIGHT  	10
#define LOW_HEIGHT		30
#define HIGH_HEIGHT		40
//Distance to obstacle parameters
#define CLOSE_DIST		30
#define FAR_DIST		35

//Throttle percent commands
#define THR_LIFT   (45+THR_OFFS)
#define THR_HOVER  (40+THR_OFFS)
#define THR_LOWER  (35+THR_OFFS)
#define THR_LAND   (30+THR_OFFS)
#define THR_OFF    0
#define THR_ARM    0
//Rudder percent commands
#define RUD_ARM    0
#define RUD_DISARM (100)
#define RUD_LEFT   (53+RUD_OFFS)
#define RUD_CENTER (50+RUD_OFFS)
#define RUD_RIGHT  (47+RUD_OFFS)
//Elevator percent commands
#define ELE_FW     (47+ELE_OFFS)
#define ELE_CENTER (50+ELE_OFFS)
#define ELE_RW     (53+ELE_OFFS)
//Aileron percent commands
#define AIL_LEFT   (47+AIL_OFFS)
#define AIL_CENTER (50+AIL_OFFS)
#define AIL_RIGHT  (53+AIL_OFFS)
#define NA        -1			//Not Applicable

//Positions in air
#define TILTEDFORWARD -1000
#define TILTEDBACKWARD 1000
#define TILTEDLEFT	  -1000
#define TILTEDRIGHT	   1000

#define ACCFORWARD	 1000
#define ACCBACKWARD -1000
#define ACCLEFTROLL	-1000
#define ACCRIGHTROLL 1000
#define ACCLEFTYAW	-1000
#define ACCRIGHTYAW	 1000

/* Public declarations */

/* Public functions */
void standBy(void);
void check(void);
void startUp(void);
void liftOff(void);
void action(void);
int searchWifi(void);
void signalWifi(void);
void land(void);
void shutOff(void);

int getThrOffs(void);
void setThrOffs(int value);
int getRudOffs(void);
void setRudOffs(int value);
int getEleOffs(void);
void setEleOffs(int value);
int getAilOffs(void);
void setAilOffs(int value);


/* Public variables */
// HwState
enum {
    StartUp    = 0,
    LiftOff    = 2,
    Action     = 3,
    SearchWifi = 4,
    Stop       = 5,
    SignalWifi = 6,
    Land       = 7,
    ShutOff    = 8,
    StandBy    = 9,
    SetError   = 10
};


#endif /* INC_DIRECTIONS_H_ */
