/*
 * pwm.h
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

#ifndef PWM_H_
#define PWM_H_

/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
void saveCommands(int T, int R, int E, int A);
void setCommands(void);

#endif /* PWM_H_ */
