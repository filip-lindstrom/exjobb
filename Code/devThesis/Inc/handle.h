/*
 * handle.h
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

#ifndef INC_HANDLE_H_
#define INC_HANDLE_H_


/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
    void handleSensors(void);
    void handleBluetooth(void);
    void handleWifi(void);
    void handleMpu(void);
    int getWifiSignal(void);
    int getGpsData(int type);
    void setError(int ErrorType);

/* Public variables */
    // Wifi states
enum {
    NoSignal   = 0,
    LowSignal  = 1,
    HighSignal = 2,
};

enum ErrorType {
	XorYOutOfBounds = 0,
};

#endif /* INC_HANDLE_H_ */
