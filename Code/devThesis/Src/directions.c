/*
 * directions.c
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

/* Private includes */
#include "main.h"

/* Private declarations */

/* Private functions */

/* Private variables */
static int Counter = 0;

//Offsets
int THR_OFFS = 0;
int RUD_OFFS = -2;
int ELE_OFFS = -3;
int AIL_OFFS = -3;

/* ------------------------------------------ */
/*
	Returns value of THR_OFFS
    \param: None
    \return: Returns current offset of Throttle
*/
/* ------------------------------------------ */
int getThrOffs(void)
{
	return THR_OFFS;
}

/* ------------------------------------------ */
/*
	Sets value of THR_OFFS to parameter value
    \param: New value for throttle offset
    \return: None
*/
/* ------------------------------------------ */
void setThrOffs(int value)
{
	THR_OFFS = value;
}

/* ------------------------------------------ */
/*
	Returns value of RUD_OFFS
    \param: None
    \return: Returns current offset of Rudder
*/
/* ------------------------------------------ */
int getRudOffs(void)
{
	return RUD_OFFS;
}

/* ------------------------------------------ */
/*
	Sets value of RUD_OFFS to parameter value
    \param: New value for rudder offset
    \return: None
*/
/* ------------------------------------------ */
void setRudOffs(int value)
{
	RUD_OFFS = value;
}

/* ------------------------------------------ */
/*
	Returns value of ELE_OFFS
    \param: None
    \return: Returns current offset of elevator
*/
/* ------------------------------------------ */
int getEleOffs(void)
{
	return ELE_OFFS;
}

/* ------------------------------------------ */
/*
	Sets value of ELE_OFFS to parameter value
    \param: New value for elevator offset
    \return: None
*/
/* ------------------------------------------ */
void setEleOffs(int value)
{
	ELE_OFFS = value;
}

/* ------------------------------------------ */
/*
	Returns value of AIL_OFFS
    \param: None
    \return: Returns current offset of Aileron
*/
/* ------------------------------------------ */
int getAilOffs(void)
{
	return AIL_OFFS;
}

/* ------------------------------------------ */
/*
	Sets value of AIL_OFFS to parameter value
    \param: New value for aileron offset
    \return: None
*/
/* ------------------------------------------ */
void setAilOffs(int value)
{
	AIL_OFFS = value;
}

/* ------------------------------------------ */
/*
    Set all commands to center
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void standBy(void)
{
    //if (DEBUG_STATE) com_c(BT, "Standing by\n\r");
    saveCommands(THR_ARM, RUD_CENTER, ELE_CENTER, AIL_CENTER);
}

/* ------------------------------------------ */
/*
    Set THR and RUD to ARM position for one sec.
    Reset Counter and move to LiftOff
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void startUp(void)
{
    if (DEBUG_STATE) com_c(BT, "Starting up \n\r");
    if (Counter < 200) {
        saveCommands(THR_ARM, RUD_ARM, ELE_CENTER, AIL_CENTER);
    }
    else {
        Counter = 0;
        setHwState(LiftOff);
    }
    Counter++;
}

/* ------------------------------------------ */
/*
    Set THR to lift command, rest to center to rise straight.
    If height goes above 95, it will move to state Action.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void liftOff(void)
{
    if (DEBUG_STATE) com_c(PC, "LiftOff\n\r");
    if (Counter < 50) {
    	saveCommands(THR_LOWER, RUD_CENTER, ELE_CENTER, AIL_CENTER);
    }
    else if (Counter < 100) {
    	saveCommands(THR_HOVER, NA, NA, NA);
    }
    else if (Counter < 150) {
    	saveCommands(THR_LOWER, NA, NA, NA);
    }
    else if (Counter < 200) {
    	saveCommands(THR_HOVER, NA, NA, NA);
    }
    else if (Counter < 1000){
		if (getDistanceDown() < LOW_HEIGHT) {
			com_ci(BT, "Counter: %d\n\r", Counter);
			saveCommands(THR_LIFT, RUD_CENTER, ELE_CENTER, AIL_CENTER);
		}
		else if (getDistanceDown() >= LOW_HEIGHT) {
			setHwState(Action);
		    Counter = 0;
		}
    }
    else setHwState(Land);
    Counter++;
}

/* ------------------------------------------ */
/*
    Handles all commands while in air.
    Limited to 2000 cycles ~ 40sec
    Then forces to switch state to Land
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void action(void)
{
    if (Counter < 3000) {
        if (DEBUG_STATE) com_c(BT, "Flying ");
        if (getDistanceDown() <= LOW_HEIGHT) {
            if (DEBUG_STATE) com_c(BT, "up\n\r");
            saveCommands(THR_LIFT, NA, NA, NA);
        }
        else if (getDistanceDown() > LOW_HEIGHT && getDistanceDown() <= HIGH_HEIGHT) {
            if (DEBUG_STATE) com_c(BT, "hover\n\r");
            saveCommands(THR_HOVER, NA, NA, NA);
        }
        else if (getDistanceDown() > HIGH_HEIGHT) {
            if (DEBUG_STATE) com_c(BT, "down\n\r");
            saveCommands(THR_LOWER, NA, NA, NA);
        }
        if (getDistanceFront() <= CLOSE_DIST) {
        	saveCommands(NA, NA, ELE_RW, NA);
        }
        else if (getDistanceFront() <= CLOSE_DIST) {
        	saveCommands(NA, NA, ELE_CENTER, NA);
        }
        if (searchWifi() == 3) {
        	com_c(BT, "Landing because of High Wifi Signal\n\r");
        	setHwState(Land);
        }
    }
    else {
        Counter = 0;
        setHwState(Land);
    }
    Counter++;
}

/* ------------------------------------------ */
/*
	Sets commands depending on Wifi-pins
    \param: None
    \return: Status of Wifi
*/
/* ------------------------------------------ */
int searchWifi(void)
{
    if (getWifiSignal() == NoSignal) {
    	return 1;
    }
    else if (getWifiSignal() == LowSignal) {
    	return 2;
    }
    else if (getWifiSignal() == HighSignal) {
        //saveCommands(THR_HOVER, RUD_CENTER, ELE_RW, AIL_CENTER);
        return 3;
    }
    else return -1;

}
/* ------------------------------------------ */
/*
    While too high it keeps one throttle value that decreases height.
    When close enough to ground, it throttles out for 20 cycles.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void land(void)
{
    if (DEBUG_STATE) com_c(BT, "Landing \n\r");
    if (getDistanceDown() >= LAND_HEIGHT) {
        saveCommands(THR_LOWER, NA, NA, NA);
    }
    else if (getDistanceDown() < LAND_HEIGHT) {
        if (Counter < 20) {
            saveCommands(THR_OFF, 0, 0, 0);
            Counter++;
        }
        else {
            Counter = 0;
            setHwState(ShutOff);
        }
    }
}

/* ------------------------------------------ */
/*
    Sets THR and RUD to DISARM positions for the FC to disarm.
    Moves on to StandBy position.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void shutOff(void)
{
    if (Counter < 200) {
        if (DEBUG_STATE) com_c(BT, "Shutting down\n\r");
        saveCommands(THR_OFF, RUD_DISARM, ELE_CENTER, AIL_CENTER);
    }
    else {
        Counter = 0;
        setHwState(StandBy);
    }
    Counter++;
}
