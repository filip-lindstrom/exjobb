/*
 * handle.c
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */


/* Private includes */
#include "main.h"

/* Private declarations */

/* Private functions */
void handleWifiLights(int state);

/* Private variables */
int8_t WifiSignal = FALSE;


/* ------------------------------------------ */
/*
    Listens for data in buffer.
    If data corresponds to s, q or d etc. it will set states to
    StartUp, ShutOff or send debug info via BT.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleBluetooth(void)
{
	// Values from UART, d, l, q, s
	enum {
		debug   = 100,	// d
		landing = 108,	// l
		quit    = 113,	// q
		start   = 115,	// s
		ThrInc  = 119,	// w
		ThrDec  = 101,	// e
		RudInc  = 114, 	// r
		RudDec  = 116, 	// t
		EleInc  = 121,	// y
		EleDec  = 117,	// u
		AilInc  = 105,	// i
		AilDec  = 111	// o
	};

	int RxBuffer = getUart2();

	// Set state to start up machine
	if (RxBuffer == start) {
		com_c(PC, "Start\n");
		setHwState(StartUp);
	}
	// Set state to turn machine off
	else if (RxBuffer == quit) {
		com_c(PC, "Quit\n");
		setHwState(ShutOff);
	}
	else if (RxBuffer == landing) {
		com_c(PC, "Landing\n");
		setHwState(Land);
	}
	// Print debug info via BT
	else if (RxBuffer == debug) {
		com_ci(BT, "Current HWstate %d \n\r", getHwState());
		com_ci(BT, "Current statemachine %d\n\r", getCurrentState());
		com_ci(BT, "DistanceDown %d\n\r", getDistanceDown());
		com_ci(BT, "Distance Front %d\n\r", getDistanceFront());
		if (WifiSignal == HighSignal) com_c(BT, "High wifi-signal\n\r");
		else if (WifiSignal == LowSignal) com_c(BT, "Low wifi-signal\n\r");
		else if (WifiSignal == NoSignal) com_c(BT, "No wifi-signal\n\r");
	}
	else if (RxBuffer == ThrInc) {
		setThrOffs(getThrOffs() + 1);
		com_ci(BT, "ThrInc : %d\n\r", getThrOffs());
	}
	else if (RxBuffer == ThrDec) {
		setThrOffs(getThrOffs() - 1);
		com_ci(BT, "ThrDec : %d\n\r", getThrOffs());
	}
	else if (RxBuffer == RudInc) {
		setRudOffs(getRudOffs() + 1);
		com_ci(BT, "RudInc : %d\n\r", getRudOffs());
	}
	else if (RxBuffer == RudDec) {
		setRudOffs(getRudOffs() - 1);
		com_ci(BT, "RudDec : %d\n\r", getRudOffs());
	}
	else if (RxBuffer == EleInc) {
		setEleOffs(getEleOffs() + 1);
		com_ci(BT, "EleInc : %d\n\r", getEleOffs());
	}
	else if (RxBuffer == EleDec) {
		setEleOffs(getEleOffs() - 1);
		com_ci(BT, "EleDec : %d\n\r", getEleOffs());
	}
	else if (RxBuffer == AilInc) {
		setAilOffs(getAilOffs() + 1);
		com_ci(BT, "AilInc : %d\n\r", getAilOffs());
	}
	else if (RxBuffer == AilDec) {
		setAilOffs(getAilOffs() - 1);
		com_ci(BT, "AilDec : %d\n\r", getAilOffs());
	}
}

/* ------------------------------------------ */
/*
    Reads on pin connected to Wifi-module and sets flag WifiSignal
    to TRUE if module has found a strong signal.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleWifi()
{
    if (HAL_GPIO_ReadPin(WifiDetected_GPIO_Port, WifiDetected_Pin) == 1) {
        if (HAL_GPIO_ReadPin(WifiNear_GPIO_Port, WifiNear_Pin) == 1) {
            // To do
            WifiSignal = HighSignal;
            if(DEBUG_WIFI) com_c(BT, "WifiNear\n");
        }
        else {
            // To do
            WifiSignal = LowSignal;
            if(DEBUG_WIFI) com_c(BT, "Wifi not near\n");
        }
    }
    else {
        WifiSignal = NoSignal;
        if(DEBUG_WIFI) com_c(BT, "No Wifi-signal\n");
    }
}

/* ------------------------------------------ */
/*
	Returns state of Wifisignal
    \param: None
    \return: State of Wifisignal
*/
/* ------------------------------------------ */
int getWifiSignal(void)
{
    return WifiSignal;
}

/* ------------------------------------------ */
/*
    Turns lights on or off to signal wifi-status
    \param: 1 if lights should be on, 0 otherwise
    \return: None
*/
/* ------------------------------------------ */
void handleWifiLights(int state)
{
	HAL_GPIO_WritePin(OnboardLed_GPIO_Port, OnboardLed_Pin, state);
}

/* ------------------------------------------ */
/*
    Reads register from GPS module and saves the data to variables.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleMpu(void)
{
	readMpuAll();
}

/* ------------------------------------------ */
/*
	Set error message to com-port and changes hw state.
    \param: Errortype
    \return: None
*/
/* ------------------------------------------ */
void setError(int ErrorType)
{
	if (ErrorType == XorYOutOfBounds) {
    	setHwState(ShutOff);
    	com_c(PC, "Error: XorYOutofBounds\n\r");
    	com_c(BT, "Error: XorYOutofBounds\n\r");
	}
}
