/*
 * state.c
 *
 *  Created on: 26 mars 2018
 *      Author: virtual
 */

/* Private includes */
#include "main.h"


/* Private declarations */

/* Private functions */
void runRead(void);
void runHandle(void);
void runHW(void);
void runInactive(void);
int getHwState(void);
int getCurrentState(void);
void setCurrentState(int value);

/* Private variables */
// CurrentState
enum {
    Read      = 0,
    Handle    = 1,
    HW        = 2,
    Inactive  = 3
};

int CurrentState  = Read;
int HwState       = StandBy;
int DistanceDown  = 0;
int DistanceFront = 0;


/* ------------------------------------------ */
/*
    Function to loop through read, handle and hw
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runStatemachine(void)
{
	runRead();
	runHandle();
	runHW();
	setCommands();
}

/* ------------------------------------------ */
/*
    Runs all necessary reading of sensors, bt, gps and wifi.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runRead(void)
{
    /* Sensor reads every 50ms, system operates at 20ms so every second cycle
       will read the value */
    static uint8_t SensorTrig = FALSE;
    SensorTrig = !SensorTrig;
    if (SensorTrig == TRUE) {
        DistanceDown = readSensors(Down);
    }
    else if (SensorTrig == FALSE) {
        DistanceFront = readSensors(Front);
    }
}

/* ------------------------------------------ */
/*
    Runs the functions connected to each read data.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runHandle(void)
{
    handleBluetooth();
    handleWifi();
    handleMpu();
}

/* ------------------------------------------ */
/*
    Makes sure that bootup, startup and shutdown all handles in the correct way.
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runHW(void)
{
    if (HwState == StandBy)         standBy();
    else if (HwState == StartUp)    startUp();
    else if (HwState == LiftOff)    liftOff();
    else if (HwState == Action)     action();
    else if (HwState == SearchWifi) searchWifi();
    else if (HwState == Land)       land();
    else if (HwState == ShutOff)    shutOff();

    if (getAccX() > 8000 || getAccX() < -8000 || getAccY() > 8000 || getAccY() < -8000) {
    	setError(XorYOutOfBounds);
    }
}

/* ------------------------------------------ */
/*
    \param: None
    \return: HwState 0-9
*/
/* ------------------------------------------ */
int getHwState(void)
{
    return HwState;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: CurrentState 0-3
*/
/* ------------------------------------------ */
int getCurrentState(void)
{
    return CurrentState;
}

/* ------------------------------------------ */
/*
    \param: HwState 0-9
    \return: None
*/
/* ------------------------------------------ */
void setHwState(int value)
{
    HwState = value;
}

/* ------------------------------------------ */
/*
    \param: CurrentState 0-3
    \return: None
*/
/* ------------------------------------------ */
void setCurrentState(int value)
{
    CurrentState = value;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Distance from sensor to ground
*/
/* ------------------------------------------ */
int getDistanceDown(void)
{

    return DistanceDown;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Distance from sensor to obstacle in front
*/
/* ------------------------------------------ */
int getDistanceFront(void)
{
    return DistanceFront;
}
