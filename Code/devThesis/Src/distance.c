/*
 * distance.c
 *
 *  Created on: 22 mars 2018
 *      Author: virtual
 */


/* Private includes */
#include "main.h"

/* Private declarations */

/* Private functions */
int filterDistanceFront(void);
int filterDistanceDown(void);

/* Private variables */
static int filterFront[FILTERSIZEFRONT];
static int filterDown[FILTERSIZEDOWN];

/* ------------------------------------------ */
/*
    Function to collect distance from both sensor
    \param: Direction of sensor, Down or Front
    \return: Distance as float from given sensor to surface
*/
/* ------------------------------------------ */
int readSensors(int Dir)
{
    if (Dir == Down) {
        return filterDistanceDown();
    }
    else if (Dir == Front) {
        return filterDistanceFront();
    }
    else return -1;
}

/* ------------------------------------------ */
/*
    Sets all positions in array to a certain value at start.
    \param: Value for array to be set as start.
    \return: First position and last positions values added.
*/
/* ------------------------------------------ */
void initFilters(int valueFront, int valueDown)
{
    for (int i = 0; i < FILTERSIZEFRONT; i++) {
        filterFront[i] = valueFront;
    }

    for (int i = 0; i < FILTERSIZEDOWN; i++) {
        filterDown[i] = valueDown;
    }
}

/* ------------------------------------------ */
/*
    Triggers ultrasonic signal, records the time it takes for
    the waves to come back. Calculates an average distance from
    last 10 readings.
    \param: None
    \return: Distance as float from front sensor to obstacle.
*/
/* ------------------------------------------ */
int filterDistanceFront()
{
	int CurrDistFront = getAdc(2) / 5;	// To make value similar to centimeters
    static int PrevDistFront = 0;
    int AveDistFront = 0;

    /* If distance is out of range, use last loops value of counter. */
    if (CurrDistFront > 200 || CurrDistFront < 0) {
        return PrevDistFront;
    }
    PrevDistFront = 0;

    /* Move all previously saved distance to the right, and therefor delete
       the oldest save. Put the latest recorded distance in first place. */
    for (int i = FILTERSIZEFRONT - 1; i >= 0; i--) {
        filterFront[i + 1] = filterFront[i];
    }

    filterFront[0] = CurrDistFront;
    /* Add all distances to counter and divide with size of filter (10).
       Returns average distance to obstacle in front of aircraft. */
    for (int i = 0; i < FILTERSIZEFRONT; i++) {
        AveDistFront += filterFront[i];
    }
    AveDistFront = AveDistFront / FILTERSIZEFRONT;
    PrevDistFront = AveDistFront;

    return AveDistFront;
}

/* ------------------------------------------ */
/*
    Triggers ultrasonic signal, records the time it takes for
    the waves to come back. Calculates an average distance from
    last 10 readings.
    \param: None
    \return: Distance as float from bottom sensor to ground.
*/
/* ------------------------------------------ */
int filterDistanceDown(void)
{
	int CurrDistDown = getAdc(1) / 5;	// To make value similar to centimeters
    static int PrevDistDown = 0;
    int AveDistDown = 0;

    /* If distance is out of range, use last loops value of counter. */
    if (CurrDistDown > 200 || CurrDistDown < 0) {
        return PrevDistDown;
    }
    PrevDistDown = 0;

    return CurrDistDown;

    /* Move all previously saved distance to the right, and therefore delete
       the oldest save. Put the latest recorded distance in first place. */
    for (int i = FILTERSIZEDOWN - 1; i >= 0; i--) {
        filterDown[i + 1] = filterDown[i];
    }

    filterDown[0] = CurrDistDown;
    /* Add all distances to counter and divide with size of filter (10).
       Returns average distance to obstacle in front of aircraft. */
    for (int i = 0; i < FILTERSIZEDOWN; i++) {
        AveDistDown += filterDown[i];
    }
    AveDistDown = AveDistDown / FILTERSIZEDOWN;
    PrevDistDown = AveDistDown;

    return AveDistDown;
}
