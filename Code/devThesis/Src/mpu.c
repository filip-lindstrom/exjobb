/*
 * mpu.c
 *
 *  Created on: 6 apr. 2018
 *      Author: virtual
 */

/* Private includes */
#include "sd_hal_mpu6050.h"
#include "main.h"

/* Private declarations */
#define GYROX_OFFS +13
#define GYROY_OFFS +26
#define GYROZ_OFFS -119
#define ACCX_OFFS  -1035
#define ACCY_OFFS  -84
#define ACCZ_OFFS  +0
#define TEMP_OFFS  -2

/* Private function */
static void MX_I2C1_Init(void);

/* Private variables */
typedef struct {
	int Temp;
	int GyroX;
	int GyroY;
	int GyroZ;
	int AccX;
	int AccY;
	int AccZ;
}tdStructMpuData;

tdStructMpuData Mpu;
I2C_HandleTypeDef hi2c1;
SD_MPU6050 mpu1;
SD_MPU6050_Result MpuResult;

/* ------------------------------------------ */
/*
    Starts up MPU and reads all data
    \param: None
    \return: Nones
*/
/* ------------------------------------------ */
bool initMpu(void)
{
	MX_I2C1_Init();
	MpuResult = SD_MPU6050_Init(&hi2c1, &mpu1,SD_MPU6050_Device_0, SD_MPU6050_Accelerometer_2G, SD_MPU6050_Gyroscope_250s );
	HAL_Delay(500);
	bool returnvalue = FALSE;

	if(MpuResult == SD_MPU6050_Result_Ok) {
		com_c(PC, "MPU OK\n\r");
		returnvalue = TRUE;
	}
	else {
		com_c(PC, "MPU NOT OK\n\r");
		return returnvalue;
	}
	readMpuAll();
	return returnvalue;
}

/* ------------------------------------------ */
/*
    Run each read-function
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void readMpuAll(void)
{
	SD_MPU6050_ReadAll(&hi2c1, &mpu1);
	Mpu.GyroX = mpu1.Gyroscope_X 	 + GYROX_OFFS;
	Mpu.GyroY = mpu1.Gyroscope_Y 	 + GYROY_OFFS;
	Mpu.GyroZ = mpu1.Gyroscope_Z 	 + GYROZ_OFFS;
	Mpu.AccX  = mpu1.Accelerometer_X + ACCX_OFFS;
	Mpu.AccY  = mpu1.Accelerometer_Y + ACCY_OFFS;
	Mpu.AccZ  = mpu1.Accelerometer_Z + ACCZ_OFFS;
}

/* ------------------------------------------ */
/*
    Read temperature from I2C
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void readMpuTemp(void)
{
	SD_MPU6050_ReadTemperature(&hi2c1,&mpu1);
	Mpu.Temp = mpu1.Temperature + TEMP_OFFS;
}

/* ------------------------------------------ */
/*
    Read gyroscopic X, Y, Z-values from I2C
    \param: None
    \return: None
*/
/* ------------------------------------------*/
void readMpuGyro(void)
{
	SD_MPU6050_ReadGyroscope(&hi2c1, &mpu1);
	Mpu.GyroX = mpu1.Gyroscope_X + GYROX_OFFS;
	Mpu.GyroY = mpu1.Gyroscope_Y + GYROY_OFFS;
	Mpu.GyroZ = mpu1.Gyroscope_Z + GYROZ_OFFS;
}

/* ------------------------------------------ */
/*
    Read acceleration in X, Y, Z from I2C
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void readMpuAcc(void)
{
	SD_MPU6050_ReadAccelerometer(&hi2c1, &mpu1);
	Mpu.AccX = mpu1.Accelerometer_X  + ACCX_OFFS;
	Mpu.AccY  = mpu1.Accelerometer_Y + ACCY_OFFS;
	Mpu.AccZ = mpu1.Accelerometer_Z  + ACCZ_OFFS;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of gyroscope in X-angle
*/
/* ------------------------------------------ */
int getGyroX(void)
{
	return Mpu.GyroX;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of gyroscope in Y-angle
*/
/* ------------------------------------------ */
int getGyroY(void)
{
	return Mpu.GyroY;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of gyroscope in Z-angle
*/
/* ------------------------------------------ */
int getGyroZ(void)
{
	return Mpu.GyroZ;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of Temperature around MPU
*/
/* ------------------------------------------ */
int getTemp(void)
{
	return Mpu.Temp;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of acceleration in X-angle
*/
/* ------------------------------------------ */
int getAccX(void)
{
	return Mpu.AccX;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of acceleration in Y-angle
*/
/* ------------------------------------------ */
int getAccY(void)
{
	return Mpu.AccY;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Value of acceleration in Z-angle
*/
/* ------------------------------------------ */
int getAccZ(void)
{
	return Mpu.AccZ;
}

/* ------------------------------------------ */
/*
    Tests where zero is at current position.
    Adds all values to a buffer and takes an
    average of that buffer for each loop.
    \param: Value of current motion
    \return: Value of acceleration in X-angle
*/
/* ------------------------------------------ */
int testAverageValue(int value)
{
	static int ix = 0;
	static int64_t temp = 0;
	temp += value;
	ix++;
	int ave = temp/ix + 1;

	com_ci(PC, "Average: %d\n\r", ave);
	return ave;
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}


