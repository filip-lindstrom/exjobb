/*
 * pwm.c
 *
 *  Created on: 22 mars 2018
 *      Author: virtual
 */


/* Private includes */
#include "main.h"

/* Private declarations */

/* Private functions */


/* Private variables */
int8_t Throttle = 0;
int8_t Rudder = 0;
int8_t Elevator = 0;
int8_t Aileron = 0;


/* ------------------------------------------ */
/*
    Converts from percent to correct PPM format.
    \param: Command in percent, 0-100%
    \return: Converted command, 1000-2000
*/
/* ------------------------------------------ */
int convertToPWM(int value)
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    return value;
}

/* ------------------------------------------ */
/*
    Sets the throttle (thrust) command to be sent
    \param: The amount of thrust in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setThrottle(int value)
{
	HAL_GPIO_WritePin(Throttle_GPIO_Port, Throttle_Pin, 1);
	delayUs(convertToPWM(value));
	HAL_GPIO_WritePin(Throttle_GPIO_Port, Throttle_Pin, 0);
}

/* ------------------------------------------ */
/*
    Sets the rudder (rotation) command to be sent
    \param: The amount of rudder in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setRudder(int value)
{
	HAL_GPIO_WritePin(Rudder_GPIO_Port, Rudder_Pin, 1);
	delayUs(convertToPWM(value));
	HAL_GPIO_WritePin(Rudder_GPIO_Port, Rudder_Pin, 0);
}

/* ------------------------------------------ */
/*
    Sets the elevator (front/back) command to be sent
    \param: The amount of elevator in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setElevator(int value)
{
	HAL_GPIO_WritePin(Elevator_GPIO_Port, Elevator_Pin, 1);
	delayUs(convertToPWM(value));
	HAL_GPIO_WritePin(Elevator_GPIO_Port, Elevator_Pin, 0);
}

/* ------------------------------------------ */
/*
    Sets the aileron (tilt to sides) command to be sent
    \param: The amount of aileron in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setAileron(int value)
{
	HAL_GPIO_WritePin(Aileron_GPIO_Port, Aileron_Pin, 1);
	delayUs(convertToPWM(value));
	HAL_GPIO_WritePin(Aileron_GPIO_Port, Aileron_Pin, 0);
}

/* ------------------------------------------ */
/*
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: All four commands in percent, 0-100%
    \return: None
*/
/* ------------------------------------------ */
void saveCommands(int T, int R, int E, int A)
{
	if (T >= 0) Throttle = T;
	if (R >= 0) Rudder   = R;
	if (E >= 0) Elevator = E;
	if (A >= 0) Aileron  = A;
}

/* ------------------------------------------ */
/*
    Calls functions for creating PWM
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param:
    \return: None
*/
/* ------------------------------------------ */
void setCommands(void)
{
	setThrottle(Throttle);
	setRudder(Rudder);
	setElevator(Elevator);
	setAileron(Aileron);
}
