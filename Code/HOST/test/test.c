/* ------------------------------------------------------ */
/*
    test.c
    Filip Lindström - filip.lindstrom@devport.se
    Autonomous drone - Examwork for DevPort

    Description: Test cases for functions made in lib-folder
*/
/* ------------------------------------------------------ */

#include "../../../../unity/src/unity.h"
#include "../lib/library.h"

void test_SetCommands(void)
{
    TEST_ASSERT_EQUAL(100, setCommands(100,50,10,20));
}

void test_ConvertToPWM(void)
{
    TEST_ASSERT_EQUAL(1000, convertToPWM(-10));
    TEST_ASSERT_EQUAL(2000, convertToPWM(200));
    TEST_ASSERT_EQUAL(1350, convertToPWM(35));
}

void test_InitFilterFront(void)
{
    TEST_ASSERT_EQUAL(40, initFilterFront(20));
    TEST_ASSERT_EQUAL(-1, initFilterFront(-5));
    TEST_ASSERT_EQUAL(200, initFilterFront(100));
    TEST_ASSERT_EQUAL(2000, initFilterFront(1000));
}

void test_FilterDistanceFront(void)
{
    initFilterFront(0);
    TEST_ASSERT_EQUAL(10, filterDistanceFront(100));
}

void test_GetGpsData(void)
{
    TEST_ASSERT_EQUAL(4, getGpsData(0));
    TEST_ASSERT_EQUAL(301, getGpsData(1));
    TEST_ASSERT_EQUAL(278, getGpsData(2));
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_SetCommands);
    RUN_TEST(test_ConvertToPWM);
    RUN_TEST(test_InitFilterFront);
    RUN_TEST(test_FilterDistanceFront);
    RUN_TEST(test_GetGpsData);
    return UNITY_END();
}
