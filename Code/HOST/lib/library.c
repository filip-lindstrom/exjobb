/* ------------------------------------------------------ */
/*
    library.c
    Filip Lindström - filip.lindstrom@devport.se
    Autonomous drone - Examwork for DevPort

    Description: Simple functions needed by main.c
*/
/* ------------------------------------------------------ */

#include "library.h"
#include "../src/main.h"
#define FILTERSIZEFRONT 10


/* ------------------------------------------ */
/*
    Converts from percent to correct PPM format.
    \param: Command in percent, 0-100%
    \return: Converted command, 1000-2000
*/
/* ------------------------------------------ */
int convert(int input)
{
    input = (input * 10);
    return input;
}


/* ------------------------------------------ */
/*
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: All four commands in percent, 0-100%
    \return: None
*/
/* ------------------------------------------ */
int setCommands(int T, int R, int E, int A)
{
    T = convert(T);
    R = convert(R);
    E = convert(E);
    A = convert(A);
    //printf("Converted T: %d\n", A);
    int Commands[5] = {0, T, R, E, A};
    int Pins[5] = {0, 1, 2, 3, 4};
    int Counter = 0;
    //printf("%d, %d, %d, %d, %d\n", Commands[0], Commands[1], Commands[2], Commands[3], Commands[4]);
    while (Counter < 4) {
        for (int i = 1; i < 5; i++) {
            if (Commands[i] < Commands[i - 1]) {
                int tempCommands, tempPins;
                // Shuffle Commands to lowest first
                tempCommands = Commands[i];
                Commands[i] = Commands[i - 1];
                Commands[i - 1] = tempCommands;
                // Shuffle Pins to same order as Commands
                tempPins = Pins[i];
                Pins[i] = Pins[i - 1];
                Pins[i - 1] = tempPins;
            }
        }
        Counter++;
    }
    /*printf("%d, %d, %d, %d, %d\n", Commands[0], Commands[1], Commands[2], Commands[3], Commands[4]);
    printf("wait(1000 + %d) Output: %d\n", Commands[1], Pins[1]);
    printf("wait(%d - %d) Output: %d\n", Commands[2], Commands[1], Pins[2]);
    printf("wait(%d - %d) Output: %d\n", Commands[3], Commands[2], Pins[3]);
    printf("wait(%d - %d) Output: %d\n", Commands[4], Commands[3], Pins[4]);
    */
    return Commands[1];
}


/* ------------------------------------------ */
/*
    Sets the aileron (tilt to sides) command to be sent
    \param: The amount of aileron in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
int setAileron(int value)
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    //AileronOut.write(1);
    //wait_us(value);
    //AileronOut.write(0);
    return value;
}

/* ------------------------------------------ */
/*
    Converts from percent to correct PPM format.
    \param: Command in percent, 0-100%
    \return: Converted command, 1000-2000
*/
/* ------------------------------------------ */
int convertToPWM(int value)
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    return value;
}

static int filterFront[FILTERSIZEFRONT];

int initFilterFront(int value)
{
    if (value < 0) return -1;

    for (int i = 0; i < FILTERSIZEFRONT; i++) {
        filterFront[i] = value;
    }
    return filterFront[0] + filterFront[FILTERSIZEFRONT - 1];
}

/* ------------------------------------------ */
/*
    Triggers ultrasonic signal, records the time it takes for
    the waves to come back. Calculates an average distance from
    last 10 readings.
    \param: None
    \return: Distance as float from front sensor to obstacle.
*/
/* ------------------------------------------ */
int filterDistanceFront(int CurrDistFront)
{
    static int PrevDistFront;
    int AveDistFront = 0;

    /* If distance is out of range, use last loops value of counter. */
    if (CurrDistFront > 300 || CurrDistFront < 0) {
        return PrevDistFront;
    }
    PrevDistFront = 0;

    /* Move all previously saved distance to the right, and therefor delete
        the oldest save. Put the latest recorded distance in first place. */
    for (int i = FILTERSIZEFRONT - 1; i >= 0; i--) {
        filterFront[i + 1] = filterFront[i];
        AveDistFront += filterFront[i];
    }

    filterFront[0] = CurrDistFront;
    AveDistFront += CurrDistFront;
    
    /* Add all distances to counter and divide with size of filter (10).
        Returns average distance to obstacle in front of aircraft. */
    AveDistFront = AveDistFront / FILTERSIZEFRONT;
    PrevDistFront = AveDistFront;

    return AveDistFront;
}

int getGpsData(int type)
{
    int compassDir = 4;
    int gpsCoordX = 301;
    int gpsCoordY = 278;
    switch(type) {
        case 0:
	    return compassDir;
	    break;
	case 1:
	    return gpsCoordX;
	    break;
	case 2: 
	    return gpsCoordY;
	    break;
	default:
	    return -1;
	    break;
    }
}
