/* ------------------------------------------------------ */
/*
    library.h
    Filip Lindström - filip.lindstrom@devport.se

    Description: Headerfile for declaration of functions in library.c
*/
/* ------------------------------------------------------ */

int initFilterFront(int value);
int filterDistanceFront(int value);
int setCommands(int T, int R, int E, int A);
int setAileron(int value);
int convertToPWM(int value);
int getGpsData(int type);
