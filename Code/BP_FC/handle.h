/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
    void handleSensors(void);
    void handleBluetooth(void);
    void handleWifi(void);
    void handleGps(void);
    int8_t getWifiSignal(void);

/* Public variables */
enum {
    NoSignal     = 0,
    LowSignal    = 1,
    HighSignal   = 2,
};
