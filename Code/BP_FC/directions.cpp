/* Private includes */
#include "main.h"
#include "state.h"
#include "directions.h"
#include "handle.h"

/* Private declarations */

/* Private functions */

/* Private variables */
static int Counter = 0;

/* ------------------------------------------ */
/*
    Set all commands to center
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void standBy(void) 
{
    if (DEBUG_STATE) com("Standing by\n");
    saveCommands(THR_ARM, RUD_CENTER, ELE_CENTER, AIL_CENTER);
}

/* ------------------------------------------ */
/*
    Not yet implemented
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void check(void)
{
   // if check = allgood
   // else 
   //     NextState = StandBy;
}

/* ------------------------------------------ */
/*
    Set THR and RUD to ARM position for one sec. 
    Reset Counter and move to LiftOff
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void startUp(void) 
{
    if (DEBUG_STATE) com("Starting up \n");
    if (Counter < 200) {
        saveCommands(THR_ARM, RUD_ARM, ELE_CENTER, AIL_CENTER);
    }
    else {
        Counter = 0;
        setHwState(LiftOff);
    }
    Counter++;
}

/* ------------------------------------------ */
/*
    Set THR to lift command, rest to center to rise straight. 
    If height goes above 95, it will move to state Action. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void liftOff(void) 
{
    if (DEBUG_STATE) com("LiftOff\n");
    if (getDistanceDown() < 95) {
        saveCommands(THR_LIFT, RUD_CENTER, ELE_CENTER, AIL_CENTER);
    }
    else if (getDistanceDown() >= 95) {
        setHwState(Action);
    }
}

/* ------------------------------------------ */
/*
    Handles all commands while in air. 
    Limited to 2000 cycles ~ 40sec
    Then forces to switch state to Land
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void action(void) 
{
    if (Counter < 2000) {
        if (getDistanceFront() <= 80) {
            if (DEBUG_STATE) com("rw \n");
            saveCommands(NA, RUD_CENTER, ELE_RW, AIL_CENTER);
        }
        else if (getDistanceFront() > 80 && getDistanceDown() <= 120) {
            if (DEBUG_STATE) com("center \n");
            saveCommands(NA, NA, ELE_CENTER, NA);
        }
        else if (getDistanceFront() > 120) {
            if (DEBUG_STATE) com("fw \n");
            saveCommands(NA, RUD_CENTER, ELE_FW, AIL_CENTER);
        }

        if (DEBUG_STATE) com("Flying ");
        if (getDistanceDown() <= 80) {
            if (DEBUG_STATE) com("up \n");
            saveCommands(THR_LIFT, NA, NA, NA);
        }
        else if (getDistanceDown() > 80 && getDistanceDown() <= 120) {
            if (DEBUG_STATE) com("hover \n");
            saveCommands(THR_HOVER, NA, NA, NA);
        }
        else if (getDistanceDown() > 120) {
            if (DEBUG_STATE) com("down \n");
            saveCommands(THR_LOWER, NA, NA, NA);
        }

        if (getWifiSignal() == LowSignal) {
            setHwState(SearchWifi);
        }
    }
    else {
        Counter = 0;
        setHwState(Land);
    }
    Counter++;
}

/* ------------------------------------------ */
/*
     
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void searchWifi(void)
{
    if (getWifiSignal() == NoSignal) {
        
    }
    else if (getWifiSignal() == LowSignal) {
        
    }
    else if (getWifiSignal() == HighSignal) {
        saveCommands(THR_HOVER, RUD_CENTER, ELE_RW, AIL_CENTER);

    }
    
}
/* ------------------------------------------ */
/*
    While too high it keeps one throttle value that decreases height. 
    When close enough to ground, it throttles out for 20 cycles. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void land(void) 
{   
    if (DEBUG_STATE) com("Landing ");
    if (getDistanceDown() >= 30) {
        saveCommands(THR_LOWER, NA, NA, NA);
    }
    else if (getDistanceDown() < 30) {
        if (Counter < 20) {
            saveCommands(THR_OFF, 0, 0, 0);
            Counter++;
        }
        else {
            Counter = 0;
            setHwState(ShutOff);
        }
    }
}

/* ------------------------------------------ */
/*
    Sets THR and RUD to DISARM positions for the FC to disarm. 
    Moves on to StandBy position. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void shutOff(void) 
{
    if (Counter < 100) {
        if (DEBUG_STATE) com("Shutting down\n");
        saveCommands(THR_OFF, RUD_DISARM, ELE_CENTER, AIL_CENTER);
    }
    else {
        Counter = 0;
        setHwState(StandBy);
    }
    Counter++;
}

/* ------------------------------------------ */
/*
    
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void setError(void) 
{
    
}
