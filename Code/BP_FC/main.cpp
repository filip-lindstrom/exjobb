/* Private includes */
#include "main.h"
#include "distance.h"
#include "directions.h"
#include "state.h"
#include "handle.h"

/* Private declarations */
DigitalOut ONLED(PC_13);

DigitalOut ThrottleOut(PA_4);
DigitalOut RudderOut(PA_5);
DigitalOut ElevatorOut(PA_6);
DigitalOut AileronOut(PA_7);

Serial pc(PA_9, PA_10, 57600);
Timer tCycle;
Timer tCommands;

/* Private functions */
void setThrottle(int value);
void setRudder(int value);
void setElevator(int value);
void setAileron(int value);
int convertToPWM(int input);
void setCommands(void); 

/* Private variables */
int8_t Throttle = 0;
int8_t Rudder = 0;
int8_t Elevator = 0;
int8_t Aileron = 0;

/* ------------------------------------------ */
/*
    Allows for serial output outside of main.cpp.  
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void com(char string[])
{
    pc.printf(string);
}

/* ------------------------------------------ */
/*
    Converts from percent to correct PPM format. 
    \param: Command in percent, 0-100%
    \return: Converted command, 1000-2000
*/
/* ------------------------------------------ */
int convertToPWM(int value) 
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    return value;
}

/* ------------------------------------------ */
/*
    Sets the throttle (thrust) command to be sent
    \param: The amount of thrust in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setThrottle(int value)
{
    ThrottleOut.write(1);
    wait_us(convertToPWM(value));
    ThrottleOut.write(0);
}

/* ------------------------------------------ */
/*
    Sets the rudder (rotation) command to be sent
    \param: The amount of rudder in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setRudder(int value)
{
    RudderOut.write(1);
    wait_us(convertToPWM(value));   
    RudderOut.write(0);  
}

/* ------------------------------------------ */
/*
    Sets the elevator (front/back) command to be sent
    \param: The amount of elevator in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setElevator(int value)
{
    ElevatorOut.write(1);
    wait_us(convertToPWM(value));   
    ElevatorOut.write(0);
}

/* ------------------------------------------ */
/*
    Sets the aileron (tilt to sides) command to be sent
    \param: The amount of aileron in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setAileron(int value)
{
    AileronOut.write(1);
    wait_us(convertToPWM(value));
    AileronOut.write(0);
}

/* ------------------------------------------ */
/*
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: All four commands in percent, 0-100%
    \return: None
*/
/* ------------------------------------------ */
void saveCommands(int T, int R, int E, int A) 
{
    if (T >= 0) Throttle = T;
    if (R >= 0) Rudder   = R;
    if (E >= 0) Elevator = E;
    if (A >= 0) Aileron  = A;
}

/* ------------------------------------------ */
/*
    Calls functions for creating PWM
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: 
    \return: None
*/
/* ------------------------------------------ */
void setCommands(void) 
{
    setThrottle(Throttle);
    setRudder(Rudder);
    setElevator(Elevator);
    setAileron(Aileron);
}
/* ------------------------------------------ */
/*
    Converts from percent to correct PPM format. 
    \param: Command in percent, 0-100%
    \return: Converted command, 1000-2000
*/
/* ------------------------------------------ */
int convertToPWM(int value) 
{
    if (value > 100) value = 100;
    if (value < 0) value = 0;
    value = (value * 10) + 1000;
    return value;
}

/* ------------------------------------------ */
/*
    Sets the throttle (thrust) command to be sent
    \param: The amount of thrust in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setThrottle(int value)
{
    ThrottleOut.write(1);
    wait_us(convertToPWM(value));
    ThrottleOut.write(0);
}

/* ------------------------------------------ */
/*
    Sets the rudder (rotation) command to be sent
    \param: The amount of rudder in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setRudder(int value)
{
    RudderOut.write(1);
    wait_us(convertToPWM(value));   
    RudderOut.write(0);  
}

/* ------------------------------------------ */
/*
    Sets the elevator (front/back) command to be sent
    \param: The amount of elevator in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setElevator(int value)
{
    ElevatorOut.write(1);
    wait_us(convertToPWM(value));   
    ElevatorOut.write(0);
}

/* ------------------------------------------ */
/*
    Sets the aileron (tilt to sides) command to be sent
    \param: The amount of aileron in percent, 0% = 1ms, 100% = 2ms
    \return: None
*/
/* ------------------------------------------ */
void setAileron(int value)
{
    AileronOut.write(1);
    wait_us(convertToPWM(value));
    AileronOut.write(0);
}

/* ------------------------------------------ */
/*
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: All four commands in percent, 0-100%
    \return: None
*/
/* ------------------------------------------ */
void saveCommands(int T, int R, int E, int A) 
{
    if (T >= 0) Throttle = T;
    if (R >= 0) Rudder   = R;
    if (E >= 0) Elevator = E;
    if (A >= 0) Aileron  = A;
}

/* ------------------------------------------ */
/*
    Calls functions for creating PWM
    T = Throttle, R = Rudder, E = Elevator, A = Aileron
    \param: 
    \return: None
*/
/* ------------------------------------------ */
void setCommands(void) 
{
    setThrottle(Throttle);
    setRudder(Rudder);
    setElevator(Elevator);
    setAileron(Aileron);
}

int main()
{
    /* ---------------SETUP-----------------*/

    /*---------------STARTUP----------------*/
    //wait(7.0);
    pc.printf("GO\n"); 
    int TimeOut = 0;
    initFilters(15, 40);
    //wait(5.0);

    /*----------------RUNNER----------------*/
    while(TimeOut < 5000) {
        tCycle.reset();
        tCycle.start();   // Start timer

        runStateMachine();
        setCommands();

        TimeOut++;

        /*----------------SERIAL-DEBUG----------------*/
        if (DEBUG_DISTANCE) {
            int distancedown = getDistanceDown();
            int distancefront = getDistanceFront();
            pc.printf("DistanceFront: %d\tDistanceDown: %d\n", distancefront, distancedown);
        }
        if (DEBUG_COMMANDS) {
            pc.printf("T: %d, R: %d, E: %d, A: %d\n", Throttle, Rudder, Elevator, Aileron);
        }
        if (DEBUG_TIME) pc.printf("TimeOut: %d\n All processes take: %d ms\n", TimeOut, tCycle.read_ms());
        /*----------------/SERIAL-DEBUG---------------*/

        while (tCycle.read_ms() < 20) {
            wait_us(10);    // As long as timer is below 20ms, wait.
        }
    }
    shutOff();
}
