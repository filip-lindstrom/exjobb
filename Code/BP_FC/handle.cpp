/* Private includes */
#include "main.h"
#include "state.h"
#include "handle.h" 
#include "directions.h"

/* Private declarations */
Serial bt(PA_2, PA_3, 57600);
DigitalIn WifiDetected(PB_6, PullDown);
DigitalIn WifiNear(PB_7, PullDown);
DigitalOut WiFiLights(PB_4);

/* Private functions */
void handleWifiLights(int state);

/* Private variables */
int8_t WifiSignal = FALSE;

/* ------------------------------------------ */
/*
    Listens for data in buffer. 
    If data corresponds to s, q or d, it will set states to 
    StartUp, ShutOff or send debug info via BT. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleBluetooth(void) 
{
    if (bt.readable()) {
        int input = bt.getc();

        // Set state to start up machine
        if (input == 's') {
            bt.printf("Start\n");
            setHwState(StartUp);
        }
        // Set state to turn machine off
        else if (input == 'q') {
            bt.printf("Quit\n"); 
            setHwState(ShutOff);
        }
        // Print debug info via BT
        else if (input == 'd') {
            bt.printf("Current HWstate %d \nCurrent statemachine-state %d \nDistance Down: %d \nDistance Front: %d\n", 
                    getHwState(), getCurrentState(), getDistanceDown(), getDistanceFront());
        }
    }
}

/* ------------------------------------------ */
/*
    Reads on pin connected to Wifi-module and sets flag WifiSignal
    to TRUE if module has found a strong signal. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleWifi()
{
    if (WifiDetected == 1) {
        if (WifiNear == 1) {
            // To do
            WifiSignal = HighSignal;
            bt.printf("WifiNear\n");
            handleWifiLights(1);
        }
        else {
            // To do
            WifiSignal = LowSignal;
            bt.printf("Wifi not near\n");
            handleWifiLights(0);
        }
    }
    else {
        WifiSignal = NoSignal;
        handleWifiLights(0);

        //pc.printf("Wifi signal is weak\n");
    } 
}

int8_t getWifiSignal(void)
{
    return WifiSignal;
}

/* ------------------------------------------ */
/*
    Turns lights on or off to signal wifi-status
    \param: 1 if lights should be on, 0 otherwise
    \return: None
*/
/* ------------------------------------------ */
void handleWifiLights(int state)
{
    WiFiLights.write(state);
}

/* ------------------------------------------ */
/*
    Not yet implemented
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void handleGps()
{
    
}
