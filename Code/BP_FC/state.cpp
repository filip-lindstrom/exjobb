/* Private includes */
#include "main.h"
#include "directions.h"
#include "state.h"
#include "distance.h"
#include "handle.h"

/* Private declarations */
DigitalOut TrigDown(PB_10, 0);
DigitalOut TrigFront(PB_11, 0);

/* Private functions */
void runRead(void);
void runHandle(void);
void runHW(void);
void runInactive(void);
int getHwState(void);
int getCurrentState(void);
void setCurrentState(int value);

/* Private variables */
int CurrentState  = Read;
int HwState       = StandBy;
int DistanceDown  = 0;
int DistanceFront = 0;

/* ------------------------------------------ */
/*
    Function to loop through read, handle and hw
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runStateMachine(void)
{
    switch(getCurrentState()) {
        case Read:
            runRead();
            setCurrentState(Handle);
        case Handle:
            runHandle();
            setCurrentState(HW);
        case HW:
            runHW();
            setCurrentState(Read);
            break;
        case Inactive:
            runInactive();
            break;
        default:
            break;
    }
}

/* ------------------------------------------ */
/*
    Runs all necessary reading of sensors, bt, gps and wifi. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runRead(void)
{   
    /* Sensor reads every 50ms, system operates at 20ms so every second cycle
       will read the value */
    static bool SensorTrig = FALSE;
    SensorTrig = !SensorTrig;
    if (SensorTrig == TRUE) {
        DistanceDown = readSensors(Down);
    }
    else if (SensorTrig == FALSE) {
        DistanceFront = readSensors(Front);
    }
}

/* ------------------------------------------ */
/*
    Runs the functions connected to each read data. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runHandle(void)
{
    handleBluetooth();
    /*handleWifi();
    handleGps();
    handleAction();
    */
}

/* ------------------------------------------ */
/*
    Makes sure that bootup, startup and shutdown all handles in the correct way. 
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runHW(void)
{
    if (HwState == StandBy) standBy();
    else if (HwState == Check) check();
    else if (HwState == StartUp) startUp();
    else if (HwState == LiftOff) liftOff();
    else if (HwState == Action) action();
    else if (HwState == SearchWifi) searchWifi();
    else if (HwState == Land) land();
    else if (HwState == ShutOff) shutOff();
    else if (HwState == SetError) setError();
}

/* ------------------------------------------ */
/*
    Not yet implemented
    \param: None
    \return: None
*/
/* ------------------------------------------ */
void runInactive(void)
{
    standBy();
}

/* ------------------------------------------ */
/*
    \param: None
    \return: HwState 0-9
*/
/* ------------------------------------------ */
int getHwState(void)
{
    return HwState;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: CurrentState 0-3
*/
/* ------------------------------------------ */
int getCurrentState(void)
{
    return CurrentState;
}

/* ------------------------------------------ */
/*
    \param: HwState 0-9
    \return: None
*/
/* ------------------------------------------ */
void setHwState(int value)
{
    HwState = value;
}

/* ------------------------------------------ */
/*
    \param: CurrentState 0-3
    \return: None
*/
/* ------------------------------------------ */
void setCurrentState(int value)
{
    CurrentState = value;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Distance from sensor to ground
*/
/* ------------------------------------------ */
int getDistanceDown(void)
{

    return DistanceDown;
}

/* ------------------------------------------ */
/*
    \param: None
    \return: Distance from sensor to obstacle in front 
*/
/* ------------------------------------------ */
int getDistanceFront(void)
{
    return DistanceFront;
}
