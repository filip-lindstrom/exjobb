/* Public includes */

/* Defines */
#define THR_LIFT   70
#define THR_HOVER  50
#define THR_LOWER  40
#define THR_LAND   30
#define THR_OFF    0
#define THR_ARM    0
#define RUD_ARM    0
#define RUD_DISARM 100
#define RUD_LEFT   70
#define RUD_CENTER 50
#define RUD_RIGHT  30
#define ELE_FW     30
#define ELE_CENTER 50
#define ELE_RW     70
#define AIL_LEFT   30
#define AIL_CENTER 50
#define AIL_RIGHT  70
#define NA        -1


/* Public declarations */

/* Public functions */
void standBy(void);
void check(void);
void startUp(void); 
void liftOff(void); 
void action(void);
void searchWifi(void);
void signalWifi(void); 
void land(void); 
void shutOff(void); 
void setError(void); 

/* Public variables */
// HwState
enum {
    StartUp    = 0,
    Check      = 1,
    LiftOff    = 2,
    Action     = 3,
    SearchWifi = 4,
    Stop       = 5,
    SignalWifi = 6,
    Land       = 7,
    ShutOff    = 8,
    StandBy    = 9, 
    SetError   = 10
};
