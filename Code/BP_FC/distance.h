/* Public includes */
#include "main.h"

/* Defines */
#define FILTERSIZEDOWN 10
#define FILTERSIZEFRONT 10

/* Public declarations */

/* Public functions */
int readSensors(int Dir);
void initFilters(int valueFront, int valueDown);
/* Public variables */
enum {
    Down  = 0,
    Front = 1
};
