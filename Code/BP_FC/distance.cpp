/* Private includes */
#include "distance.h"

/* Private declarations */
AnalogIn UsDown(PA_1);
AnalogIn UsFront(PA_0);

/* Private functions */
int filterDistanceFront(int value);
int filterDistanceDown(int value);

/* Private variables */
static int filterFront[FILTERSIZEFRONT];
static int filterDown[FILTERSIZEDOWN];

/* ------------------------------------------ */
/*
    Function to collect distance from both sensor
    \param: Direction of sensor, Down or Front
    \return: Distance as float from given sensor to surface
*/
/* ------------------------------------------ */
int readSensors(int Dir)
{    
    if (Dir == Down) {
        return filterDistanceDown(UsDown.read() * 1000);
    }
    else if (Dir == Front) {
        return filterDistanceFront(UsFront.read() * 1000);
    }
    else return -1;
}

/* ------------------------------------------ */
/*
    Sets all positions in array to a certain value at start. 
    \param: Value for array to be set as start.
    \return: First position and last positions values added. 
*/
/* ------------------------------------------ */
void initFilters(int valueFront, int valueDown)
{
    for (int i = 0; i < FILTERSIZEFRONT; i++) {
        filterFront[i] = valueFront;
    }
    
    for (int i = 0; i < FILTERSIZEDOWN; i++) {
        filterDown[i] = valueDown;
    }
}

/* ------------------------------------------ */
/*
    Triggers ultrasonic signal, records the time it takes for
    the waves to come back. Calculates an average distance from 
    last 10 readings. 
    \param: None
    \return: Distance as float from front sensor to obstacle.
*/
/* ------------------------------------------ */
int filterDistanceFront(int CurrDistFront)
{
    static int PrevDistFront;
    int AveDistFront = 0;

    /* If distance is out of range, use last loops value of counter. */
    if (CurrDistFront > 300 || CurrDistFront < 0) {
        return PrevDistFront;
    }
    
    return CurrDistFront;
    
    /* Move all previously saved distance to the right, and therefor delete
        the oldest save. Put the latest recorded distance in first place. */
    for (int i = FILTERSIZEFRONT - 1; i >= 0; i--) {
        filterFront[i + 1] = filterFront[i];
        AveDistFront += filterFront[i];
    }

    filterFront[0] = CurrDistFront;
    AveDistFront += CurrDistFront;
    
    /* Add all distances to counter and divide with size of filter (10).
        Returns average distance to obstacle in front of aircraft. */
    AveDistFront = AveDistFront / FILTERSIZEFRONT;
    PrevDistFront = AveDistFront;

    return AveDistFront;
}

/* ------------------------------------------ */
/*
    Triggers ultrasonic signal, records the time it takes for
    the waves to come back. Calculates an average distance from 
    last 10 readings. 
    \param: None
    \return: Distance as float from bottom sensor to ground.
*/
/* ------------------------------------------ */
int filterDistanceDown(int CurrDistDown)
{
    static int PrevDistDown;
    int AveDistDown = 0;

    /* If distance is out of range, use last loops value of counter. */
    if (CurrDistDown > 300 || CurrDistDown < 0) {
        return PrevDistDown;
    }
    PrevDistDown = 0;

    //return CurrDistDown;

    /* Move all previously saved distance to the right, and therefor delete
        the oldest save. Put the latest recorded distance in first place. */
    for (int i = FILTERSIZEDOWN - 1; i >= 0; i--) {
        filterDown[i + 1] = filterDown[i];
        AveDistDown += filterDown[i];

    }

    filterDown[0] = CurrDistDown;
    AveDistDown += CurrDistDown;

    /* Add all distances to counter and divide with size of filter (10).
        Returns average distance to obstacle in front of aircraft. */
    AveDistDown = AveDistDown / FILTERSIZEDOWN;
    PrevDistDown = AveDistDown;

    return AveDistDown;
}
