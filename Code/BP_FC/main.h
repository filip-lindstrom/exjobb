/* Public includes */
#include "../mbed-STM32F103C8T6/stm32f103c8t6.h"
#include "mbed.h"

/* Defines */
#define TRUE 1
#define FALSE 0
#define DEBUG_DISTANCE 1
#define DEBUG_STATE 0
#define DEBUG_TIME 0
#define DEBUG_COMMANDS 0

/* Public declarations */

/* Public functions */
void saveCommands(int T, int R, int E, int A);
void com(char string[]);

/* Public variables */
