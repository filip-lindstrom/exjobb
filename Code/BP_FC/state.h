/* Public includes */

/* Defines */

/* Public declarations */

/* Public functions */
void runStateMachine(void);
void setHwState(int value);
int getHwState(void);
int getCurrentState(void);
int getDistanceDown(void);
int getDistanceFront(void);

/* Public variables */

// CurrentState
enum {
    Read      = 0,
    Handle    = 1,
    HW        = 2,
    Inactive  = 3
};
