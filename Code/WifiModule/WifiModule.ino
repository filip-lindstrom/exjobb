/*
 *  This sketch demonstrates how to scan WiFi networks. 
 *  The API is almost the same as with the WiFi Shield library, 
 *  the most obvious difference being the different file you need to include:
 */
#include "ESP8266WiFi.h"
#define NETWORKSSID "HUAWEI_Honor8"
#define BUILTINLED D0
#define FOUND D5
#define STRENGTH D6
#define YELLOWLED D7
#define REDLED D8

void setup() {
  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  pinMode(FOUND, OUTPUT);
  pinMode(STRENGTH, OUTPUT);
  pinMode(YELLOWLED, OUTPUT);
  pinMode(REDLED, OUTPUT);
  pinMode(BUILTINLED, OUTPUT);
  digitalWrite(BUILTINLED, 1);
  Serial.println("Setup done");
}

void loop() {
  Serial.println("scan start");
  
  // WiFi.scanNetworks will return the number of networks FOUND
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  int FOUNDSSID = 0;
  int32_t Rssi = 0;
  if (n == 0)
    Serial.println("no networks FOUND");
  else
  {
    Serial.print(n);
    Serial.println(" networks FOUND");
    for (int i = 0; i < n; ++i)
    {
    // Print NETWORKSSID and Rssi for each network FOUND
      if (WiFi.SSID(i) == NETWORKSSID) {
        FOUNDSSID = 1;
        Rssi = WiFi.RSSI(i);
      }
    }
    if (FOUNDSSID == 0) {
      Serial.print("Couldn´t find ");
      Serial.println(NETWORKSSID);
      digitalWrite(FOUND, 0);
      digitalWrite(STRENGTH, 0);
      digitalWrite(REDLED, 0);
      digitalWrite(YELLOWLED, 0);      
      digitalWrite(BUILTINLED, 1);   
    }
    else if (FOUNDSSID = 1 && Rssi < -55) {
      Serial.print("FOUND low signal SSID with Rssi: ");
      Serial.println(Rssi);
      digitalWrite(FOUND, 1);
      digitalWrite(STRENGTH, 0);
      digitalWrite(REDLED, 0);
      digitalWrite(YELLOWLED, 1);
      digitalWrite(BUILTINLED, 1);
    }
    else if (FOUNDSSID = 1 && Rssi >= -55) {
      Serial.print("FOUND high signal SSID with Rssi: ");
      Serial.println(Rssi);
      digitalWrite(FOUND, 1);
      digitalWrite(STRENGTH, 1);
      digitalWrite(REDLED, 1);
      digitalWrite(YELLOWLED, 1);
      digitalWrite(BUILTINLED, 0);
    }
  }
  Serial.println("");

  // Wait a bit before scanning again
  //delay(2000);
}
