# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?
Contains all code and documentation for the thesis project at DevPort AB called AvaDrone.

An autonomous drone flies until it finds a Wifi-network and then lands as close to it as possible.

##### Quick summary
All source code for GCC-ARM toolchain is placed within Code/devthesis.

In Code/BP_FC is a Keil project which was left after that project size hit the roof of the evaluation software.

Code/HOST contains unit-tests and environment for developing and testing on PC.

Code/WifiModule holds the Arduino-code for the NodeMCU WiFi-module.

##### Version
0.1

##### [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

##### Configuration

##### Dependencies
[ SK450 Manual ](https://hobbyking.com/media/file/173646057X79294X45.pdf)

[ STM32F103C8 Datasheet ](http://www.st.com/content/ccc/resource/technical/document/datasheet/33/d4/6f/1d/df/0b/4c/6d/CD00161566.pdf/files/CD00161566.pdf/jcr:content/translations/en.CD00161566.pdf)

[ MaxBotics LV-EZ0 / LV-EZ1](https://www.maxbotix.com/documents/LV-MaxSonar-EZ_Datasheet.pdf)

[ NodeMCU ](https://nodemcu.readthedocs.io/en/master/en/)

##### Database configuration

##### How to run tests
In terminal or cmd, navigate to `C:/../exjobb/code/` and run command "make test".

This will compile the code and create an executable file named test.exe(Windows) or test.out(Linux).

Now in terminal or cmd, type test.exe or test.out and press enter.
If all tests are marked as PASS, all tests have passed.
If any test is marked as FAILED, there may be more than one faulty function, as it stops testing at first fail.

##### Deployment instructions

### Contribution guidelines

##### Writing tests
Write extential tests as below, where TEST_ASSERT_EQUAL means that integers will be compared. Can be extended with ...EQUAL_FLOAT, ...EQUAL_DOUBLE and so on to compare other datatypes. In below example, 19 is the oracle, expected answer.
The test calls function named "addition" with two inputs, 17 and 2.
If `function()` returns 19 based on input 17 and 2, the test will pass, otherwise not.

test_checkAddition is the name of below test function.  

Multiple tests can be written in the same testfunction.

    void test_checkAddition(void)
    {
        TEST_ASSERT_EQUAL(19, addition(17,2));
    }


> Add the test function in main method in test.c as done below:

    RUN_TEST(test_checkAddition);

##### Code review
By Paul Nedstrand April 18 2018

Documentation/CodeReview/april18

##### Other guidelines

### Who do I talk to?

##### Repo owner or admin
Filip Lindstr�m

filip.lindstrom@devport.se

##### Other community or team contact
[ DEVPORT.se ](DevPort.se)
